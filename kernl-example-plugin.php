<?php
/**
 * Plugin Name: Kernl Example Plugin
 * Plugin URI: https://kernl.us
 * Description: The Kernl Plugin for testing.
 * Version: 4.0.7
 * Author: Jack Slingerland
 * Author URI: http://re-cycledair.com
 */

require 'kernl-update-checker/kernl-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://kernl.us/api/v1/updates/5544bd7e5b8ae0fc1fa5e7a5/',
    __FILE__,
    'kernl-example-plugin'
);
$MyUpdateChecker->collectAnalytics = true;
$MyUpdateChecker->license = 'abc123';
?>